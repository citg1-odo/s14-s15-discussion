package com.zuitt.discussion.config;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtToken implements Serializable {

//    to get token from application.properties
    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    private UserRepository userRepository;

    private static final long serialVersionUID = -1223079690548090747L;

//    time duration in seconds that the token can be used
    public static final long JWT_TOKEN_VALIDITY = 5*60*60; // 5 hours token validity, units are in seconds

    private String doGenerate(Map<String, Object> claims, String subject){
        // Jwts.builder() creates a new JWT builder instance. This object is used to build and sign the JWT.
        // .setClaims includes the information to show the recipient which is the username
        // .setSubject adds information about the subject. (The subject username.)
        // .setIssuedAt sets the time and date when the token was created
        // .setExpiration sets the expiration of the token
        // .signWith creates the token using a declared algorithm, with the secret keyword.
        // "HS512" is a secure cryptographic algorithm.
        // The "secret key" is passed as a parameter and is used to verify the signature later on.
        // .compact() is used to generate the final JWT string by compacting the JWT builder object.

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public String generateToken(UserDetails userDetails){
        Map<String, Object> claims = new HashMap<>();

//      retrieves the user information from the userRepository by using the username from the UserDetails object
        User user = userRepository.findByUsername(userDetails.getUsername());

//        id is added to the claims of JWT to be used to identify the user in the server
        claims.put("user", user.getId());

        return doGenerate(claims, userDetails.getUsername());
    }

//    Token validation by extracting the username from the token
    public Boolean validateToken(String token, UserDetails userDetails){
//        Extract the username from the token and store it in a variable
        final String username = getUsernameFromToken(token);

//        first condition checks if the username in the userDetails matches the username extracted from the token
//        second condition checks if the token is expired by invoking isTokenExpired() method
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

//  This method extracts a specific claim from a JWT token by using functional interface.
    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver){
        final Claims claims = getAllClaimsFromToken(token);

//        this extracts the specific claim value
        return claimsResolver.apply(claims);
    }

//    this extracts all the claims from the token
    private Claims getAllClaimsFromToken(String token){
        return Jwts.parser()//is used to parse a JWT Token
                .setSigningKey(secret) //sets the secret key used to sign token
                .parseClaimsJws(token) //actually parses the JWT Token
                .getBody();  //contains all the claims that were in the JWT
    }

//    extract the subject(username) from the JWT
    public String getUsernameFromToken(String token){
        String claim = getClaimFromToken(token, Claims::getSubject);
        return claim;
    }

//    used to extract expiration date of token
    public Date getExpirationDateFromToken(String token){
        return getClaimFromToken(token, Claims::getExpiration);
    }

    private Boolean isTokenExpired(String token){
        final Date expiration = getExpirationDateFromToken(token);
//        this returns a boolean value that represents whether the expiration date is before the current time
        return expiration.before(new Date());
    }
}
