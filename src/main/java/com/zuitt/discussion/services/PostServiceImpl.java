//contains the business logic concerned with a particular object in the class
package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.PostRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service //allow us to use the CRUD repository methods inherited from the CRUDRepository
public class PostServiceImpl implements PostService{
    // an object cannot be instantiated from interfaces
    // @Autowired allow us to use the interface as if it was an instance of an object
    // and allows us to use the methods from the CrudRepository
    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtToken jwtToken;

    public void createPost(String stringToken, Post post){
//        retrieve the User object using the extracted username from JWT Token
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);
        postRepository.save(newPost);
    }

    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }

    public ResponseEntity deletePost(Long id, String stringToken) {
        Post postForDeleting = postRepository.findById(id).get();
        String postAuthorName = postForDeleting.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if (authenticatedUserName.equals(postAuthorName)){
            postRepository.deleteById(id);
            return new ResponseEntity<>("Post deleted successfully.", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("You are not authorized to delete this post.", HttpStatus.UNAUTHORIZED);
        }

    }

    public ResponseEntity updatePost(Long id, String stringToken, Post post){
        Post postForUpdate = postRepository.findById(id).get();
//        getting the author of the specific post
        String postAuthorName = postForUpdate.getUser().getUsername();
//        get the username from the token to compare it with the postAuthorName
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

//        check if the postAuthorName matches with authenticatedUserName
        if(authenticatedUserName.equals(postAuthorName)){
            postForUpdate.setTitle(post.getTitle());
            postForUpdate.setContent(post.getContent());
            postRepository.save(postForUpdate);
            return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("You are not authorized to edit this post.", HttpStatus.UNAUTHORIZED);
        }

    }

    public Iterable<Post> getMyPosts(String stringToken){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return author.getPosts();
    }
}
