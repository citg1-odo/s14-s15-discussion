package com.zuitt.discussion.models;

import java.io.Serializable;

/*
	The "JwtResponse" model will be used for sending the generated JWT token string as a response in the "AuthController".
		Add the following code inside the JwtResponse.java (Class)
*/
public class JwtResponse implements Serializable {
    private static final long serialVersionUID = 7705085851456516159L;
    private final String jwttoken;

    public JwtResponse(String jwttoken) {
        this.jwttoken = jwttoken;
    }

    public String getToken() {
        return jwttoken;
    }
}
