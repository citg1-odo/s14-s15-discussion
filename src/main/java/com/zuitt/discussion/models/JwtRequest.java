package com.zuitt.discussion.models;


import java.io.Serializable;

/*
	The "JwtRequest" model to be used in authenticating the user via request in the "AuthController"
	and for generating JWT token using the request body contents for the payload.
		Add the following code inside the JwtRequest.java (Class)
*/
public class JwtRequest implements Serializable {

    private static final long serialVersionUID = 5682712319526821315L;
    private String username;
    private String password;

    public JwtRequest() {
    }

    public JwtRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
